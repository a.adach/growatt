<?php

declare(strict_types=1);

namespace AdachSoft\Growatt;

use AdachSoft\Growatt\DTO\RequestDTO;
use AdachSoft\Growatt\DTO\ResponseDTO;
use AdachSoft\Growatt\Model\HTTPClientInterface;
use DateTime;

class GrowattApiClient
{
    private const SERVER_URL = 'https://server.growatt.com/';

    /**
     * @var HTTPClientInterface
     */
    private $httpClient;

    public function __construct(HTTPClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param string $user
     * @param string $password
     * @param string $lang
     * @return ResponseDTO
     * @throws HTTPClientException
     */
    public function login(string $user, string $password, string $lang = 'en'): ResponseDTO
    {
        $data = [
            'account' => $user,
            'password' => $password,
            'validateCode' => '',
            'isReadPact' => '0',
            'lang' => $lang,
        ];

        return $this->sendRequest('login', $data);
    }

    public function getInvTotalData(string $plantId): ResponseDTO
    {
        $data = [
            'plantId' => $plantId,
        ];

        return $this->sendRequest('indexbC/inv/getInvTotalData', $data);
    }

    public function getInvEnergyDayChart(string $plantId, DateTime $day): ResponseDTO
    {
        $data = [
            'plantId' => $plantId,
            'date' => $day->format('Y-m-d'),
        ];

        return $this->sendRequest('indexbC/inv/getInvEnergyDayChart', $data);
    }

    public function getYearEnergyChart(string $plantId): ResponseDTO
    {
        $data = [
            'plantId' => $plantId,
        ];

        return $this->sendRequest('indexbC/getYearEnergyChart', $data);
    }

    public function getPlantData(string $plantId, string $type): ResponseDTO
    {
        $data = [
            'plantId' => $plantId,
            'type' => $type,
        ];

        return $this->sendRequest('logbC/totalReport/getPlantData', $data);
    }

    public function getPlantTotalData(string $plantId, string $type, DateTime $day): ResponseDTO
    {
        $data = [
            'plantId' => $plantId,
            'type' => $type,
            'date' => $day->format('Y-m-d'),
        ];

        return $this->sendRequest('logbC/totalReport/getPlantTotalData', $data);
    }

    public function getPlantList(int $currPage, int $pageSize, string $plantName): ResponseDTO
    {
        $data = [
            'currPage' => $currPage,
            'pageSize' => $pageSize,
            'plantName' => $plantName,
        ];

        return $this->sendRequest('plantbC/plants/getPlantList', $data);
    }

    public function getTotalData(string $plantId): ResponseDTO
    {
        $data = [
            'plantId' => $plantId,
        ];

        return $this->sendRequest('indexbC/getTotalData', $data);
    }

    public function getCurrentUser(string $sessionKey): ResponseDTO
    {
        $data = [
            'sessionKey' => $sessionKey,
        ];

        return $this->sendRequest('setbC/getCurrentUser', $data);
    }

    private function sendRequest(string $path, array $data): ResponseDTO
    {
        $requestDTO = new RequestDTO(
            $this->getUrl($path), 
            'POST', 
            $data
        );

        return $this->httpClient->sendRequest($requestDTO);
    }

    private function getUrl(string $path): string
    {
        return static::SERVER_URL . $path;
    }
}
