<?php

declare(strict_types=1);

namespace AdachSoft\Growatt\Model;

use AdachSoft\Growatt\DTO\RequestDTO;
use AdachSoft\Growatt\DTO\ResponseDTO;
use AdachSoft\Growatt\Exception\HTTPClientException;

interface HTTPClientInterface
{
    /**
     * @param RequestDTO $requestDTO
     * @return ResponseDTO
     * @throws HTTPClientException
     */
    public function sendRequest(RequestDTO $requestDTO): ResponseDTO;
}
