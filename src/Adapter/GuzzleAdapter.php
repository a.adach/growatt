<?php

declare(strict_types=1);

namespace AdachSoft\Growatt\Adapter;

use AdachSoft\Growatt\DTO\RequestDTO;
use AdachSoft\Growatt\DTO\ResponseDTO;
use AdachSoft\Growatt\Exception\HTTPClientException;
use AdachSoft\Growatt\Model\HTTPClientInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException as GuzzleClientException;
use GuzzleHttp\Exception\RequestException;

final class GuzzleAdapter implements HTTPClientInterface
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(?Client $client = null)
    {
        if ($client instanceof Client){
            $this->client = $client;
            return;
        }
        $this->client = new Client(['cookies' => true]);
    }

    /**
     * @param RequestDTO $requestDTO
     * @return ResponseDTO
     * @throws HTTPClientException
     */
    public function sendRequest(RequestDTO $requestDTO): ResponseDTO
    {
        try {
            $result = $this->client->request(
                $requestDTO->getHttpMethod(),
                $requestDTO->getUrl(), 
                [
                    'form_params' => $requestDTO->getData(),
                    'header' => $requestDTO->getHeaders(),
                ]
            );
        } catch(GuzzleClientException $e){
            throw new HTTPClientException($e->getMessage());
        } catch(RequestException $e) {
            throw new HTTPClientException($e->getMessage());
        }

        $responseDTO = new ResponseDTO(
            $result->getBody()->getContents(),
            $result->getStatusCode(),
            $result->getHeaders()
        );

        return $responseDTO;
    }
}
