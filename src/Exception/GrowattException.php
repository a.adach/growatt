<?php

declare(strict_types=1);

namespace AdachSoft\Growatt\Exception;

use Exception;

class GrowattException extends Exception
{
    public const INVALID_VALUE = 0;
}
