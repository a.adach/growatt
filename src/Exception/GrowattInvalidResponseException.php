<?php

declare(strict_types=1);

namespace AdachSoft\Growatt\Exception;

use AdachSoft\Growatt\DTO\ResponseDTO;
use Exception;

class GrowattInvalidResponseException extends Exception
{
    /**
     * @var ResponseDTO
     */
    private $responseDTO;

    public function __construct(ResponseDTO $responseDTO, string $message = "Invalid response", int $code = 0)
    {
        parent::__construct($message, $code);
    }
    
    public function getResponseDTO(): ResponseDTO
    {
        return $this->responseDTO;
    }
}
