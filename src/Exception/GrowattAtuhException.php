<?php

declare(strict_types=1);

namespace AdachSoft\Growatt\Exception;

use Exception;

class GrowattAtuhException extends Exception
{
    public const USERNAME_PASSWORD_ERROR = 0;

    public const SESSION_ID_NOT_FOUND = 1;
}
