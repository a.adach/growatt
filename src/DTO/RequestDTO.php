<?php

declare(strict_types=1);

namespace AdachSoft\Growatt\DTO;

class RequestDTO
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $httpMethod;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var string[]
     */
    private $headers = [];

    public function __construct(string $url, string $httpMethod, array $data = [], array $headers = [])
    {
        $this->url = $url;
        $this->httpMethod = $httpMethod;
        $this->data = $data;
        $this->headers = $headers;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getHttpMethod(): string
    {
        return $this->httpMethod;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }
}
