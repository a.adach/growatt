<?php

declare(strict_types=1);

namespace AdachSoft\Growatt\DTO;

class ResponseDTO
{
    /**
     * @var string
     */
    private $contents;

    /**
     * @var int
     */
    private $statusCode;

    /**
     * @var string[]
     */
    private $headers = [];

    public function __construct(string $contents, int $statusCode, array $headers = [])
    {
        $this->contents = $contents;
        $this->statusCode = $statusCode;
        $this->headers = $headers;
    }

    public function getContents(): string
    {
        return $this->contents;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }
}
