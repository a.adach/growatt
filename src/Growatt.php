<?php

declare(strict_types=1);

namespace AdachSoft\Growatt;

use AdachSoft\Growatt\Exception\GrowattAtuhException;
use AdachSoft\Growatt\Exception\GrowattException;
use AdachSoft\Growatt\Exception\GrowattInvalidResponseException;
use AdachSoft\Toolbox\Json\Json;

class Growatt
{
    /**
     * @var GrowattApiClient
     */
    private $growattApiClient;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var null|string
     */
    private $sessionId;

    /**
     * @var null|string
     */
    private $plantId;

    public function __construct(string $user, string $password, GrowattApiClient $growattApiClient)
    {
        $this->user = $user;
        $this->password = $password;
        $this->growattApiClient = $growattApiClient;
        $this->sessionId = null;
    }

    public function setSessionId(?string $sessionId): void
    {
        $this->sessionId = $sessionId;
    }

    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    public function setPlantId(?string $plantId): void
    {
        $this->plantId = $plantId;
    }

    public function getPlantId(): ?string
    {
        return $this->plantId;
    }

    /**
     * @throws GrowattAtuhException
     * @throws GrowattInvalidResponseException
     * @throws GrowattException
     * @throws JsonException
     */
    public function getInvTotalData(): array
    {
        $this->loginIfNeeded();
        if (empty($this->getPlantId())) {
            throw new GrowattException('PlantId not set');
        }

        $responseDTO = $this->growattApiClient->getInvTotalData($this->getPlantId());

        return Json::decode($responseDTO->getContents());
    }

    /**
     * @throws GrowattAtuhException
     * @throws GrowattInvalidResponseException
     * @throws GrowattException
     * @throws JsonException
     */
    public function getTotalData(): array
    {
        $this->loginIfNeeded();
        if (empty($this->getPlantId())) {
            throw new GrowattException('PlantId not set');
        }

        $responseDTO = $this->growattApiClient->getTotalData($this->getPlantId());

        return Json::decode($responseDTO->getContents());
    }

    /**
     * @throws GrowattAtuhException
     * @throws GrowattInvalidResponseException
     * @throws JsonException
     */
    private function loginIfNeeded()
    {
        if (!empty($this->sessionId)) {
            return;
        }

        $responseDTO = $this->growattApiClient->login($this->user, $this->password);
        $responseData = Json::decode($responseDTO->getContents());

        if ($responseDTO->getStatusCode() !== 200) {
            throw new GrowattInvalidResponseException($responseDTO);
        }

        if (!isset($responseData['result'])) {
            throw new GrowattInvalidResponseException($responseDTO);
        }

        if ($responseData['result'] !== 1) {
            throw new GrowattAtuhException($responseData['msg']);
        }

        $headers = $responseDTO->getHeaders();
        $this->sessionId = $this->findSessionId($headers);
        if (empty($this->sessionId)) {
            throw new GrowattAtuhException('Session id not found', GrowattAtuhException::SESSION_ID_NOT_FOUND);
        }

        $this->plantId = $this->findPlantId($headers);
    }

    private function findPlantId(array $headers): ?string
    {
        if (!isset($headers['Set-Cookie'])) {
            return null;
        }
        
        foreach($headers['Set-Cookie'] as $header) {
            if (preg_match('/onePlantId=([^;]+)\;/i', $header, $m)) {
                return $m[1];
            }
        }

        return null;
    }

    private function findSessionId(array $headers): ?string
    {
        if (!isset($headers['Set-Cookie'])) {
            return null;
        }

        foreach($headers['Set-Cookie'] as $header) {
            if (preg_match('/JSESSIONID=([^;]+)\;/i', $header, $m)) {
                return $m[1];
            }
        }

        return null;
    }
}
