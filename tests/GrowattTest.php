<?php

declare(strict_types=1);

namespace Tests\AdachSoft\Growatt;

use AdachSoft\Growatt\DTO\ResponseDTO;
use AdachSoft\Growatt\Exception\GrowattAtuhException;
use AdachSoft\Growatt\Growatt;
use AdachSoft\Growatt\GrowattApiClient;
use PHPUnit\Framework\TestCase;

class GrowattTest extends TestCase
{
    public function testShouldThrowExceptionWhenUsernamePasswordError(): void
    {
        $this->expectException(GrowattAtuhException::class);

        $user = 'user';
        $pass = 'pass';

        $responseDTO = new ResponseDTO('{"result":-2,"msg":"Username Password Error"}', 200, []);

        $growattApiClient = $this->createMock(GrowattApiClient::class);
        $growattApiClient->expects($this->once())->method('login')->with($user, $pass)->willReturn($responseDTO);

        $growatt = new Growatt($user, $pass, $growattApiClient);

        $growatt->getInvTotalData();
    }

    public function testShouldThrowExceptionWhenNoSessionId(): void
    {
        $this->expectException(GrowattAtuhException::class);
        $this->expectExceptionCode(GrowattAtuhException::SESSION_ID_NOT_FOUND);
        $user = 'user';
        $pass = 'pass';

        $responseDTO = new ResponseDTO('{"result":1}', 200, []);

        $growattApiClient = $this->createMock(GrowattApiClient::class);
        $growattApiClient->expects($this->once())->method('login')->with($user, $pass)->willReturn($responseDTO);

        $growatt = new Growatt($user, $pass, $growattApiClient);
        $this->assertNull($growatt->getSessionId());

        $growatt->getInvTotalData();
    }

    public function testReturnSessionId(): void
    {
        $user = 'user';
        $pass = 'pass';

        $responseDTO = new ResponseDTO('{"result":1}', 200, $this->dataHeaderSuccess());
        $responseDTO1 = new ResponseDTO('{"result":1,"obj":{"epvToday":"0","epvTotal":"5235.4","pac":"0.1"}}', 200, []);

        $growattApiClient = $this->createMock(GrowattApiClient::class);
        $growattApiClient->expects($this->once())->method('login')->with($user, $pass)->willReturn($responseDTO);
        $growattApiClient->expects($this->once())->method('getInvTotalData')->with('335026')->willReturn($responseDTO1);

        $growatt = new Growatt($user, $pass, $growattApiClient);
        $this->assertNull($growatt->getSessionId());
        $this->assertNull($growatt->getPlantId());

        $growatt->getInvTotalData();

        $this->assertSame('2B1168383JB3D14033F92C0381AA4D72', $growatt->getSessionId());
        $this->assertSame('335026', $growatt->getPlantId());
    }

    private function dataHeaderSuccess(): array
    {
        return [
            'Set-Cookie' => [
                'JSESSIONID=2B1168383JB3D14033F92C0381AA4D72; Path=/; HttpOnly',
                'plantSize=1; Expires=Thu, 10-Nov-2022 06:03:35 GMT; Path=/',
                'onePlantId=335026; Expires=Thu, 10-Nov-2022 06:03:35 GMT; Path=/',
                'onePlantType=1; Expires=Thu, 10-Nov-2022 06:03:35 GMT; Path=/',
                'isloginValidCode=""; Expires=Thu, 01-Jan-1970 00:00:10 GMT; Path=/',
                'SERVERID=1d133ffa15079b5878f6a64999a8baac|1668060091|1668060091;Path=/',
            ],
        ];
    }
}
